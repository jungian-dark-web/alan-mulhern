# Healing of Emotional Wounds

#### A Jungian Approach
* [S1.Ep 1. Healing of emotional wounds - a Jungian approach](https://alanmulhern.podbean.com/e/healing-of-emotional-wounds-a-jungian-approach/) - [Video](https://www.youtube.com/watch?v=JrELrE7yBtw) - [Transcript](healing_emotional_wounds_Introduction.md)
  > Psychotherapy and Healing - a Jungian ApproachThere are a wide range of emotional wounds which are a damage to the sense of self. Just as there are numerous healing mechanisms in the body likewise there is a wide range of healing energies in the psyche. It is of considerable benefit to the practitioner to be familiar with these. An overview is given of the podcasts to follow which include a deep investigation of the healing intelligence in the psyche and the role of the therapist in this process. Prepare to be stretched beyond your traditional trainings and conceptions of how emotional healing takes place.The image accompanying these podcasts is The Sower and the Seed which was drawn by Lindsey C. Harris

#### Healing Intelligence as a Natural Force

* [S1.Ep 2. Healing intelligence as a natural force](https://alanmulhern.podbean.com/e/healing-intelligence-as-a-natural-force/) - [Video](https://www.youtube.com/watch?v=m6Pfwx22FWw)
  > Healing energies are a natural intelligence within the psyche promoting the unconscious state of wholeness we call health. This health is, for the most part, intensely desired by those who suffer pain, division, and conflict within themselves; for some it is the central goal of life to recover it. Those who wish to heal the wounds of others as well as their own should know there are different methods to heal psychological distress.

#### Stages of Psychotherapy as the Context for Healing

* [S1.Ep 3. The stages of psychotherapy as the context for healing](https://alanmulhern.podbean.com/e/the-stages-of-psychotherapy-as-the-context-for-healing/) - [Video](https://www.youtube.com/watch?v=SESZzoiuqo8)
  > Healing takes place in a context. We construct rituals that provide this. Other disciplines provide their own way of encouraging the healing to take place. In this podcast I outline the Jungian context and based on this my version of the stages of psychotherapy.

#### Style and Essence of Jungian Therapy

* [S1.Ep 4 Style and essence of Jungian therapy](https://alanmulhern.podbean.com/e/the-style-of-jungian-therapy/) - [Video](https://www.youtube.com/watch?v=wHn6taO_lfQ)
  > Jungian therapy leans towards a collaboration with the unconscious rather than just grasping its significance. The style of therapy is therefore only analytic in parts and favours a movement of the ego towards the Self. The essence of the therapy lies in the transcendent function - the new centre that emerges which is positioned on the ego-Self axis and facilitates the interaction of both consciousness and the unconscious.

### Alignment to the Deep Psyche

* [S1.Ep 5 The alignment to the deep psyche](https://alanmulhern.podbean.com/e/the-alignment-to-the-deep-psyche/) - [Video](www.youtube.com/watch?v=tCYe0vQaguY)
  > A method of alignment, a descent, to the deep psyche is described and a case study provided to demonstate how the “scan” can work. This is a mixture of Oriental and Western technique.

#### Contacting the Deep Psyche
* [S1.Ep 6.  A method of contacting the deep psyche](https://alanmulhern.podbean.com/e/a-method-of-contacting-the-deep-psyche/)
  > The stages of contacting the deep psyche are explained as a practical method for all therapists. This method, starting with inner awareness, proceeds to encounter the area of suffering. One area of the deep psyche (the heart chakra in Hindu philosophy) can experience healing by another (the brow chakra). This is compatible with Western psychotherapeutic knowledge of body centres and focussing awareness.

#### Inter-subjective Field Between Therapist and Client
* [S1. Ep 7. The inter-subjective field between therapist and client](https://alanmulhern.podbean.com/e/the-inter-subjective-field-between-therapist-and-client/) - [Video](https://www.youtube.com/watch?v=wsNwECSufmE)
  > Let us presume that much of the early, more reductive work such as the therapeutic alliance, the analysis of character and family history and so forth has been established. However the alignment to the deep psyche and integration has special considerations of its own and is rather different from the earlier stages.

#### Atuning to the Field  
* [S1.Ep 8.  Attuning to the field](https://alanmulhern.podbean.com/e/the-inter-subjective-field/)
  > The task of therapists in attuning to the inter-subjective field is: 1. To attune to the deep psyche of the client.2. To help locate and express wounds as fully as possible.3. To help clarify, challenge, and where possible, clear out the negativity within the psyche of the client.​4. To stimulate natural healing energy and, where required, contribute their own to aid this process within the client.

#### Integration

##### Evaluating Psychological Material
* [S1.Ep 9.  Integration step 1: Evaluating the material from the psyche](https://alanmulhern.podbean.com/e/integration-step-1_evaluating-the-material-form-the-psyche/)
  > Integration is the least developed stage in the theory and practice of psychotherapy. Here we examine the first of three steps in this process: the evaluation of healthy or unhealthy material from the psyche.

##### The Ability to Work with Contents of the Psyche
* [S1.Ep 10.  Integration step 2: the ability to work with the contents of the psyche](https://alanmulhern.podbean.com/e/integration-step-2/)
  > Here we deal with the second part of the integration process - the capacity to work psychologically with the material from the unconscious. Case studies of the varying capacities to work psychologically are given. An unfavourable psychological attitude thus prevents the process of integration even beginning. So what then is a favourable psychological attitude?

##### What it Means to Work Psychologically
* [S1.Ep 11. Integration continued: what it means to work psychologically](https://alanmulhern.podbean.com/e/integration-continued-what-it-means-to-work-psychologically/)
  > Eight dimensions of the capacity to work psychologically are examined. These are: the capacity for introspection	 the ability to express emotion and their truth	 to see oneself and others more objectively	 acceptance of self-responsibility	 awareness that one’s psyche consists of different but inter-related parts	 the conscious attitude is aligned to the unconscious but is also relatively independent	 the existence of a symbolic attitude	 the ability to contact the contents of the deep psyche with the faculty of inner awareness.

##### Movement to a New Centre of the Psyche
* [S1.Ep 12. Integration and the movement to a new centre of the psyche](https://alanmulhern.podbean.com/e/integration-and-the-movement-to-a-new-centre-of-the-psyche/)
  > Integration implies character re-formation, a change in identity structure. This requires greater access to the underlying emotional structure underpinning the ego, which undergoes change thereby stimulating character reform and development. What begins and sustains this change is the power of awareness. Self-reflectivity is the essence of the process. This is the normal but extra-ordinary process of psychotherapy, which seeks, therefore, to reinforce self-awareness by becoming more conscious of the underlying emotional structure, its complexes, difficulties, and healing possibilities. The work of personal development necessitates an intense process of integration which takes time, effort, and considerable courage. Quick solutions are rarely available, especially when the emotional wounds are serious.

##### A Wound that Won't Heal
* [S1.Ep 13. The wound that won’t heal](https://alanmulhern.podbean.com/e/the-wound-that-wont-heal/) - [Video](https://www.youtube.com/watch?v=zecGvGH851Y)
  > Therapy is not always successful; the deep psyche is not always accessible and even when it is, healing is not guaranteed. Transpersonal vision may be possible but the psyche may not have the capacity to integrate its meaning and energy. Sometimes, parts of the emotional structure are damaged and cannot heal, no matter how heroic the efforts.

##### The Potency of the Healing Force in the Psyche
* [S1.Ep 14.  The potency of the healing force in the psyche](https://alanmulhern.podbean.com/e/the-potency-of-the-healing-force-in-the-psyche/) - [Video](https://www.youtube.com/watch?v=dZiASLx2AOA)
  > The vitality of healing intelligence determines the capacity of a wounded area to heal itself. This energy may be thought of as potency two characteristics of which are:Expressiveness - the capacity of the psyche, suffering a wound, complex or trauma to express its pain in a healthy and effective manner.Receptivity - the capacity of the wounded psyche to listen and be open.While healing intelligence is an expression of the wholeness of the psyche, it is intimately bound up with numerous healthy character components, some of which are constitutional, some the result of early conditioning, some the result of training, and others Self-forged.

#### Healing Intelligence
##### Conciousness and the Ego

* [S1. Ep 15.  Healing intelligence, consciousness and the ego](https://alanmulhern.podbean.com/e/healing-interlligence-consciousness-and-the-ego/) - [Video](https://www.youtube.com/watch?v=y4Mo9MVs3_k)
  > Healing intelligence is integral to the complete functioning of the healthy psyche. It requires the cooperation of consciousness itself, which, when orientated towards healing, is aligned to the deeper psyche, learns from it, but can question it and engage in a healthy dialogue which is transformative for both consciousness and the unconsciousness.The practitioner needs to be aware of the crucial yet variable importance of the ego and of consciousness at each stage of the transformation process and healing journey and be able to judge whether the ego and its defences need challenge or support. However, it is their challenging and dismantling which leads to healing intelligence being released.

##### The Subtle Body
* [S1.Ep 16.  Healing and the subtle body/psyche](https://alanmulhern.podbean.com/e/healing-and-the-subtle-bodypsyche/) - [Video](https://www.youtube.com/watch?v=Cx3VB_UGPnk)
  > The role of the therapist in the healing process is rarely one of distance and anonymity. It is one respecting proper clinical boundaries but there are important times when there is co-evolution and mixing of psyches at a deep level and healing intelligence is activated, stimulated and shared. It is useful to think of the Self, the inner directing centre of an individual, as having shared-field properties – not a personal possession but a manifestation of the collective unconscious

##### How Healing Intelligence Works and How we Resist It
* [S1.Ep 17.  How healing Intelligence works in the psyche and how we resist it.](https://alanmulhern.podbean.com/e/how-healing-interlligence-works-in-the-psyche-and-how-we-resist-it/)
  > The cosmic principle of the opposites of creation and destruction, life and death as illustrated in Hindu metaphysics with the dancing Shiva is played out in psyche in many ways of which healing intelligence versus the forces that undermine it is one manifestation.Six ways in which healing works in the psyche are described. There are also numerous impediments in our psyche to this wonderful intelligence functioning properly: twelve are outlined.

#### Empathic Resonance
* [S1.Ep 18.  Empathic resonance in the field of therapist and client](https://alanmulhern.podbean.com/e/empathic-resonance-in-the-field-of-therapist-and-client/)
  > There may emerge unusual features once the inter-subjective field is activated. A field is an area of influence and a shared field is where two psyches, apparently separate, have temporarily dropped their boundaries and a non-verbal sharing of experience, sometimes in the form of images, sometimes with voices, takes place. This is a manifestation of healing, not belonging to an individual psyche; it is the intelligence that organises the field itself.It is often said that therapists should have empathy. But what is this capacity to enter into the emotions of another and feel things from their point of view? And are there different grades or qualities of empathy? This podcast will provide a perspective and suggest some answers to this important question.

#### Integration of the Spirit
* [S1.Ep 19.  Integration of the Spirit in Psychotherapy](https://alanmulhern.podbean.com/e/integration-of-the-spirit-in-psychotherapy/) - [Video](https://www.youtube.com/watch?v=kWhVle6WzBA)
  > The sweetness of the desert breezeYou can’t grasp in your hands.She’s like the waters of the Nile,She comes from other lands.                From: The Sower and the Seed - Isis poem

#### Questions and Response
* [S1.Ep 20.  Questions and Responses on Healing - 1](https://alanmulhern.podbean.com/e/questions-and-responses-on-healing-1/) - [Video](https://www.youtube.com/watch?v=5X6dvtFbhqg)
  > My initial question (how does healing work in psychotherapy?) and subsequent search has really turned into a longer series of other questions each of which could be broken down into further questions themselves. Let me add the obvious. The ultimate answer will never be given; but like the pearl of great price it might be intuited, approximated, and hopefully lived. Our knowledge in this field is necessarily limited; one can only reach so far into the unconscious, like wading out from the shore of an ocean.
* [S1.Ep 21.  Questions and Responses on healing - 2](https://alanmulhern.podbean.com/e/questions-and-responses-2-on-the-nature-of-healing-intelligence/) - [Video](https://www.youtube.com/watch?v=AZhYvq7tX-A)
  > THE NATURE OF HEALING INTELLIGENCE Can religions be integrated with a spiritually orientated psychotherapy? Are there psychological skills favouring healing? What is the role of the psychotherapist in this healing process? Is psychotherapy necessary for healing  to take place? Can psychotherapy block the healing process? Can all wounds be healed? What is the wounded healer? Can psychotherapy benefit those on a spiritual path? How is healing connected with individuation and wholeness? What has love and death got to do with it?

#### Secret of the Golden Flower
* [S1.Ep 22.  The Secret of the Golden Flower 1](https://alanmulhern.podbean.com/e/the-secret-of-the-golden-flower-1/) - [Video](https://www.youtube.com/watch?v=CWubsiTrfdI)
  > The history, metaphysics, and spirtual position of this ancient Taoist text is described. It has an ancient Chinese style which is inherently mysterious, symbolic, and elusive yet it has within its pages a secret of spirtual transformation that is inherently facinating and has stood the test of time.        “Silently thou fliest upward in the morning”.
* [S1.Ep 23.  The Secret of the Golden Flower 2](https://alanmulhern.podbean.com/e/the-secret-of-the-golden-flower-2/) - [Video](https://www.youtube.com/watch?v=fofAOBOc0LQ)
  > We explore the connection between this ancient Taoist meditation text and the school of Carl Jung. Despite it being an esoteric text it contains a wonderful meditation guide and an entrance into the philosophy of the Far East. It has much to offer modern psychotherapy in so far as it has a transpersonal dimension and takes seriously the proposition that spiritual practice is of great value in the healing of emotional wounds.
