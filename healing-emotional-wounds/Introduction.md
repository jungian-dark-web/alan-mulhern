# Healing of Emotional Wounds Podcast - Introduction

[S1.Ep 1. Healing of emotional wounds - a Jungian approach](https://alanmulhern.podbean.com/e/healing-of-emotional-wounds-a-jungian-approach/) - [Video](https://www.youtube.com/watch?v=JrELrE7yBtw)

### Transcript

0:08  
Hello, welcome to the introductory podcast to the series, the healing of emotional wounds. 

0:15  
My name is Alan Mulhern. I'm a working psychotherapist based in London. I was trained in the methods of Carl Gustav Jung. And it was especially in the manner in which the rich symbolic nature of the deeper psyche was portrayed by the school that most attracted me, at first, as well as its mythological, spiritual and forward looking components. These were to deeply influenced my practice as a psychotherapist, and underlying my approach to healing in these podcasts. 

#### The $64,000 question - What is Healing?

0:47  
What I have to say about healing of course, has theoretical concepts. But these are based on experience in the consulting room and observing carefully what promoted and what impeded healing and my clients In my early years as a therapist, I was once at a conference, in which some distinguished young in psychotherapists were giving an overview of the principles guiding their practice. However, the question of what is healing in psychotherapy did not arise. So, I asked how they believed healing worked. None were inclined to reply until one remarked that is "the $64,000 question". And if I had the answer to it, I would retire to the hills of Hollywood. General laughter followed. Clearly, healing was not on the agenda for serious analysts, as they prefer to be called [individuation](https://en.wikipedia.org/wiki/Individuation). Yes. Analysis, certainly. But healing, not quite. This was associated with alternative practices, perhaps charlitans... image rather than substance, it was certainly mysterious. The matter, however, remained with me as a crucial issue in psychotherapy. 

2:09  
This series of podcasts tackles this unanswered question. To express in words something of the healing process was the task I set myself. Not an easy one, since most of the work of healing in the psyche takes place deep in the unconscious. At this point in my early career, when I asked "the $64,000 question", I had reached the limits of my understanding of how psychotherapy worked as a healing tool. I was blocked from doing deep work with my clients, and my capacity to help them had reached this limits. 

#### Complexes Unable to Heal Through Analytic Understanding

2:43  
I now realize that I was largely stuck in the first stages of psychotherapy, creating a container, listening closely. Understanding the symptom picture, allowing catharsis to take place, Dream analysis, identifying the underlying Conditions underneath the symptoms, helping clients understand their problems approaching the unconscious symbolically and so important work yes, but limited. I became aware that many [complexes](https://frithluton.com/articles/complex/), as we call the emotional core of our suffering, could not be healed by analytic understanding. 

3:21  
Accordingly, I read the works of Carl Jung again, studied my clients more closely and broaden my study to include a wider range of spiritual and visionary material. Realizing that the concern with healing of emotional and spiritual wounds have been central to humanity from its beginning, and have a great deal of the world's religions and wisdom traditions have been concerned with this enormously important issue. I also came to realize that as well as my clients, the societies we emerged from, also need healing. 

#### Mysteries of Healing

3:57  
We shall begin our exploration with psychotherapy as it normally practiced. But as we proceed, the true purpose of these talks will emerge, which is to share a knowledge of healing. That is rarely communicated. Many practitioners will benefit from a more explicit expression of the mysteries of healing. Such knowledge has always been uncommon, if not esoteric, with the expansion of psychotherapy and counseling of all descriptions, such knowledge of healing is actually more difficult to find. 

4:29  
Many training programs are lighter in their requirements for personal therapy of their trainees, and there is greater demand for shorter evidence based practice. Hardly the conditions to explore the enigmatic concept of healing intelligence.

4:47  
There are no methods described here, I have not tried many times in my work, and on myself, all have been tried by others before me, by one means or another, by other psychotherapists, who also reached there. Discipline practitioners and spiritual groups and alternative healing practices. And in some cases, these techniques have been practiced for thousands of years in the Orient, and found to be a value. Nothing has been invented simply as an idea. They are based on real experience in practice, and have proved to be effective.

5:23  
The truism that one can only promote healing in another. To the extent one is healed oneself is half the story. But that does not make clear how healing actually happens. Even after some degree of personal healing, its dynamics may remain mostly unconscious. I needed to work for many years as a practitioner, before I could begin to express in words a little bit mystery. Even when my ideas became clearer, and my work more effective, it was a great challenge to articulate it more coherently. One needs therefore not only to experience His oneself, but to struggle with it, put it into practice and attempt to articulate it. 

#### Healing Intelligence

6:07  
The structure of these talks is taken from my book on [healing intelligence](https://goo.gl/y4F71r), which I felt impelled to write to clarify my thoughts in this area. The talks will cover the following topics: What is the nature of healing? What are emotional wounds? The stages of psychotherapy and the emergence of a spiritual dimension, which can be central to the healing process. 

6:30  
Next, we look at the alignment of consciousness to the deep psyche. This explores how ego consciousness can contact the deep levels of the psyche, then therapist and client in the deep psyche, important to this relationship is explored. We then look at the integration process. its essence is the full acceptance of all layers of feeling concerning wounds and complexes to realize these layers except and then integrate them which build them into one's consciousness so that they are always accessible, not buried, denied, or split off. 

7:08  
Then we move on to reflections on healing intelligence. Let me tell you a little more about this. Here we consider the importance of the potency and vitality have this extraordinary intelligence, likening it to a liquid light. I take these terms from cranial psychotherapy, a parallel discipline. We also in this section, elaborate on six specific ways in which healing intelligence operates in the psyche, such as, how wounded areas may heal themselves, or one area of the psyche may heal another. How an intelligence deep in the psyche can reorient a consciousness. So a healing environment is created within the psyche and how higher or transcendent forces in the psyche may effect radical healing. 

8:02  
We end the section with 12 major obstacles to the operation of healing intelligence. Therapist and practitioners of all descriptions, as well as those who want to heal themselves will benefit from this close knowledge of how healing intelligence works and how it is impeded.

8:21  
The next section we look at is integration with the spirit.

8:23  
Most of psychotherapy does not have an explicitly spiritual dimension, and most clients do not come to a therapist for spiritual matters, but do several reasons of painful emotional suffering. Nevertheless, a spiritual dimension can provide the vital healing and transformative ingredient required for progress. We examine the role of spirit very carefully following the teachings of Carl Jung, that the natural growth towards wholeness is inbuilt in the human psyche and is at the core of its spiritual nature.

#### Reflections

8:59  
Our final podcast is on questions and reflections on healing. Here we cover 40 of the most important questions concerning healing intelligence. This is not exactly a q&a session, since there are few definitive answers to the process, but I do give concise reflections on the questions given. 

9:18  
Examples these questions include, how does healing intelligence work? is it available to everybody? What is this connection to human suffering? Does healing require a conscious engagement with the soul? What is its connection to human darkness in the psyche? And can healing be achieved alone? Are the defenses or resistance to healing? And by what energy are these resistance is shifted? How is integration vital to the healing process? And can Eastern methods such as meditation and chakra work? be integrated with psychotherapy on the spiritual dimension Are there particular psychological aptitudes or skills favoring the work of psychotherapy and healing? And can psychotherapy also be a block to the healing process? Can all wounds be healed? A very important question. What is the wounded healer? In what ways can those on the spiritual path benefit from psychotherapy? Are there one or many sources of healing in the psyche? How is healing connected with [individuation](https://en.wikipedia.org/wiki/Individuation) and wholeness? What does love and death, finally got to do with it?

10:35  
This series of talks will take place on a weekly basis. They concern the micro aspects of healing how it works with individuals. After this there will be a longer series entitled The Quest which will explore the great visionaries across the ages, who have reoriented the division of humanity. This too is a wider healing journey but on the macro-scale. For more details, you may access more details on the website, [Alanmulhern.com](https://alanmulhern.com). 

11:07  
If you are a practitioner or training in psychotherapy, and wish to take your thinking beyond traditional training or you are looking for a journey of self discovery, perhaps you are interested in the connection between healing and spirituality or you are in another healing discipline and wish to reflect on the essence of the healing process or you wish for more knowledge of how to heal your own wounds. Then I hope you will join me in these talks. I intend to condense my experience in this field, I can think of nothing better than sharing it with like minded individuals.

11:44  
Our series of talks will begin with what is the nature of healing and what are emotional wounds.

