# Alan Mulhern

[Homepage](https://www.alanmulhern.com) - [Linkedin](https://www.linkedin.com/in/alan-mulhern-172968a9/) - [Youtube](https://www.youtube.com/channel/UC09ay5PGgpjxX3Zkw1D0GBg) - [on Karnac Books](https://www.karnacbooks.com/Author.asp?AID=18642)
  * [Psychotherapy](https://www.alanmulhern.com/psychotherapy)
  * [The World in Crisis. The Human Prospect.](https://www.alanmulhern.com/talks-world-crisis)
    > The human prospect has been increasingly framed in materialist terms which has produced increased growth and rising standards of living. Yet this has led to the possibility of our extinction.
  * [Visions for the 21st Century.](https://www.alanmulhern.com/talks-world-crisis) (scroll down) - This series examines the crises of our times and its responding  visions. 

### Podcast Index

You can find Alan Mulhern's podcast at the following address: [alanmulhern.podbean.com](https://alanmulhern.podbean.com/).

Here, I've begun to make a directory about his work, as a learning experience and making it more accessible to others.

#### [Healing of Emotional Wounds](healing-emotional-wounds/)

> Season One - Psychotherapy: Jungian Approach to Healing. This explores the healing of emotional wounds. Going beyond traditional trainings - of great value to those interested in this field. 

#### [Quest Series](quest-series.md)

> Season Two - The Quest. An exploration of great visionaries who have shaped our history and contemporary world. With special focus on the evolving crises of the 21st century.

## Books

* [Healing Intelligence: The Spirit in Psychotherapy - Working with Darkness and Light](https://www.karnacbooks.com/product/healing-intelligence-the-spirit-in-psychotherapy-working-with-darkness-and-light/32090/)
  > Natural healing intelligence is one of the great mysteries of the psyche. It is inherently elusive yet lies at the core of all efforts to cure emotional wounds. Psychotherapy and counselling, when done in depth, pass beyond interpretation to work directly with this powerful force. This book is intended to help those who suffer such emotional wounds by illuminating the path of healing as well as to provide deep insight and effective methods for the practitioner.
* [The Sower and the Seed: Reflections on the Development of Consciousness](https://www.karnacbooks.com/product/the-sower-and-the-seed-reflections-on-the-development-of-consciousness/36595/)
  > The Sower and the Seed explores the origins of consciousness from a mytho-psychological angle. The concept of immanence, a vast intelligence within the evolutionary process, provides the underlying philosophy of the book, presented as a creative-destructive spirit that manifests higher orders of complexity (such as life, intelligence, self-consciousness) and then dissolves them. The book explores the human psyche as immersed in nature and the realm of the Great Mother, showing how the themes of fertility and power, applicable to all life forms, saturate the history of humanity - most evidently in the period stretching from 40,000 years ago up to modern civilizations. The book examines in particular the transition to patriarchal religious consciousness, in which a violent separation from the world of nature took place.
  >
  > The Sower and the Seed also explores Hebrew, Egyptian, and Greek creation myths as commentaries on the origins of consciousness, and shows how the transition to consciousness from the unconscious (the birth of humanity) is inherently problematic, since it creates a separation from the realm of nature and instinct. This rise of consciousness out of nature and its fall or separation into a separated egoic state is its central dilemma.
* [Cognitive Behavioural Therapy in Mental Health Care: Second Edition](www.karnacbooks.com/product/cognitive-behavioural-therapy-in-mental-health-care-second-edition/29588/)
  > This second edition provides an accessible and thorough overview of the practice of CBT within mental health care. Updates and additions include:
  > 
  > - Revised chapters on the therapeutic relationship and case formulation
  > - New material on personality disorders and bipolar disorder
  > - New material on working with diversity
  > - Content on the multidisciplinary context of CBT, the service user perspective, CBT from a holistic perspective
  > - Developments within the cognitive behavioural psychotherapies
  > - Continous professional development for the CBT practitioner
  > - Photocopiable worksheets linked to case studies.
  > 
  > Already a tried-and-tested guide for trainee psychologists and psychotherapists, as well as clinicians in mental health services and private practices, this text is also of value to practitioners who need refresher courses in CBT.

