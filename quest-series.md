# The Quest Series  

* [Introduction to the Quest Series](https://www.alanmulhern.com/quest)
  > The Quest examines a series of great visionaries across the ages. The aim is to help participants formulate their own vision by engaging in critical study and through their inner journey.
  > 
  > The series includes not only key texts but also some great composers, poets and painters. Beside mystical texts there will also be some of the most powerful political, philosophical, religious and economic visions that have shaped humanity. A series of lectures and discussion groups will accompany the study programme given once a month in London by myself. It is possible to attend these in person or receive downloads of the talks and therefore attend the series anywhere in the world. 

[![Introduction to the Quest Series on Youtube](http://i.imgur.com/rNs29Vp.png)](https://www.youtube.com/watch?v=76nQr30Lln0)

### Search for Vision

* [S2.Ep 1. The Quest Series: The Search for Vision in the Evolving Crises of our Times](https://alanmulhern.podbean.com/e/outline-of-the-quest-series/) - [video](https://youtu.be/Tb1-_gfVkng)
  > The Quest Season about to begin will examine the great visionaries across the ages in order to seek vision for the crises of our times. These are termed the eight horseman of the apocalypse: the conflict of ideas, the economic, political, military, social, ecological and spiritual crises as well as the alteration of human consciousness as it becomes fused with artificial intelligence. The human race stands at a crossroads or at a dead end. Just as individuals suffer wounds and traumas and need healing, so too with the collective and planetary dimensions. We search the world’s wisdom traditions in order to heal the wounds of our age and seek meaning and direction in the evolving crises of the 21st century.

### Mysticism and Evolution
* [S2.Ep 2. The Quest: Mysticism and Evolution](https://alanmulhern.podbean.com/e/s2ep-1-the-quest-mysticism-and-evolution/) - [video](https://youtu.be/qEe7Hrx8NcI)
  > This is the first episode in a series that explores great visionaries from across the ages who have changed our history and contemporary world. This episode examines Pierre Teilhard de Chardin - an obscure Jesuit priest of the 20th century who gave a blazing vision of the miracle of evolution and offers an alternative view to the materialist world paradigm which has led us, sleepwalking, into our current crisis.

### James Lovelock: Visionary of Ecological Crisis
* [S2.Ep 3.  James Lovelock: Visionary of the Ecological Crisis. Part 1](https://alanmulhern.podbean.com/e/s2ep-3-james-lovelock-visionary-of-the-ecological-crisis-of-our-times/) - [video](https://youtu.be/ITWqUi5PyUc)
  > The first of two podcasts on James Lovelock. The evolving ecological crisis is of such gravity as to constitute collective suicide by humanity. This first podcast looks at the theory and evidence.
* [S2.Ep 4. James Locklock:  Ecological visionary of our time. Part 2](https://alanmulhern.podbean.com/e/ecological-visionary-of-our-time-james-locklock-part-2/) - [video](https://youtu.be/aCKuptV8rkE)
  > “If it be now, tis not to come;  if it be not to come, it will be now;  if it be not now, yet it will come. The readiness is all”. (Hamlet:Act 5 Sc.2)

### Richard Dawkings: The God Delusion

* [S2. Ep 5. Richard Dawkins:The God Delusion](https://alanmulhern.podbean.com/e/richard-dawkinsthe-god-delusion/) - [video](https://youtu.be/CBfXGc0ljIo)
  > We critically explore a humanist vision that argues there is almost certainly no God, that evolution and natural selection are the only plausible explanations for the universe and ourselves, that religions have caused enormous damage, and that our ethical systems should be forged from reason. “When one person suffers a delusion it is called insanity. when many people suffer a delusion it is called religion”. Richard Dawkins.

### Jung
#### Wounded Visionary
* [S2.Ep 6. Jung: The Wounded Visionary](https://alanmulhern.podbean.com/e/s2ep-6-jung-part-1-the-wounded-visionary/) - [video](https://youtu.be/7zSO02iPFBY)
  > Jung and the recovery of the soul: the first of a series on Carl Jung, his life, thought, formation of a school of psychotherapy, and contribution to Western civilization.

#### Introverted Mystic
* [S2.Ep 7. Jung: Introverted Mystic](https://alanmulhern.podbean.com/e/jung-introverted-mystic/) - [video](https://youtu.be/z2XmIet6ruQ)
  > We examine Chapters 8 and 9 of Memories,Dreams, Reflections which tell of the building of his Tower at Bolligen, Lake Zurich, and his travels to find a non-European perspective on himself and Western cilvilization.

#### Visions, Journey of the Soul
* [S2.Ep 8. Jung: Visions, Journey of the Soul](https://alanmulhern.podbean.com/e/s2ep-8-jung-part-3-visions-journey-of-the-soul/) - [video](https://youtu.be/6WbLT8ctAho)
  > The depths of the psyche irrupted in Jung with tremendous force. But he found that ore from which he alchemised the gold of his creations. It came from an inexhaustible seam where psyche means cosmos and buried so deep and under such pressure that it was dangerous to approach. But his daemon drove him and he unravelled the secret of bringing it to the surface, at first hot white and liquid but then transformed through symbol on its long journey from the interior.

#### Contributions to Psychotherapy
* [S2 Ep 9. Jung’s Contributions to the Psychotherapy of our Time](https://alanmulhern.podbean.com/e/s2-ep-9-alchemy-of-transformationjungs-contributions-to-the-psychotherapy-of-our-time/) - [video](https://youtu.be/nQwn1n9khn0) - [video](https://youtu.be/NznuWCW-Hng)
  > From his disintegration, his descent to the underworld, Jung brought creative treasures, not simply as theoretical ideas but as practical tools that could be inserted into the living tissue of our times. Jungian psychotherapy is a potent tool of transformation." The years … when I pursued the inner images were the most important time of my life. Everything else is to be derived from this. My entire life consisted in elaborating what had burst forth from the unconscious and flooded me like an enigmatic stream and threatened to break me. That was the stuff and material for more than only one life. Everything later was merely the outer classification, the scientific elaboration, and the integration into life. But the numinous beginning, which contained everything was then.” CGJung

### Jung and Alchemy
* [S2 Ep 10. Jung and Alchemy Part A.](https://alanmulhern.podbean.com/e/s2-ep10-jung-and-alchemy-part-a/) - [video](https://youtu.be/Dxj0rrXvWcw)
  > Jung did an enormous amount to breathe the spirit back into alchemy after the cold hand of the Western scientific enlightenment had extinguished its flame. He was a pioneer in re-interpreting the alchemical symbols into the language of archetypal myths as well as psychology allowing them to live again.He was convinced it provided a missing link between the modern analytical psychology that he had created and the wisdom of the ancient world. For him it was the language of the lost soul. Although deeply enigmatic there was a key to the alchemy mysteries and Jung was destined, vocationally called, redemptively compelled to find it as few others could. One is reminded of a saying of the gnostic redeemer: “I wandered through worlds and generations … all the mysteries unlock.”
* [S2 Ep 11. Jung and Alchemy Part B](https://alanmulhern.podbean.com/e/s2-ep11-jung-and-alchemy-part-b/) - [video](https://youtu.be/xjfaOf-dCQY)
  > “The experiences of the alchemists were, in a sense, my experiences, and their world was my world. Thus, I had at last reached the ground which underlay my own experiences of the years 1913 to 1917 (that is the descent or confrontation with the unconscious); for the process through which I had passed at that time corresponded to the process of alchemical transformation”. CGJung

#### Jung and Gnosticism

* [S2 Ep 12 Jung and Gnosticism Part A](https://alanmulhern.podbean.com/e/s2-ep-11-jung-and-gnosticism-part-a/) - [video](https://youtu.be/eTCcYMeqoQo)
  > “When I began to understand alchemy I realised that it represented the historical link with Gnosticism and that a continuity therefore existed between past and present. Grounded in the natural philosophy of the Middle Ages, alchemy formed the bridge on the one hand into the past, to Gnosticism, and on the other into the future, to the modern psychology of the unconscious. … The possibility of a comparison with alchemy, and the uninterrupted intellectual chain back to Gnosticism, gave substance to my psychology.” CGJung
* [S2 Ep 13. Jung and Gnosticism Part B](https://alanmulhern.podbean.com/e/s2-ep-13-jung-and-gnosticism-part-b/) - [video](https://youtu.be/n4zFQv0xjqI)
  > Jung underwent an initiation in his confrontation with the unconscious	 it was the gnostic spirit and voice that inhabited him in the 1913-1917 descent. The Gnostics lived a very powerful mystic tradition and there are many metaphysical and mythological ideas which form a bridge to the transpersonal aspects of Jungian psychology. Their influence on Jung was immense since he experienced gnosis through their spirit.

##### Initiation and Gnosis
* [S2 Ep 14. Initiation and Gnosis - a personal experience](https://alanmulhern.podbean.com/e/s2-ep14-initiation-and-gnosis/) - [video](https://youtu.be/QGFuhNwCBXM)
  > The essence of the gnostic experience is one that will never disappear and will again play its part in the crises that are evolving in the 21 century as many individuals undertake the path of initiation into higher knowledge, Gnosis. Such initiation rites are preceded by a period of preparation, a descent, a confrontation and an integration with the shadow, a meeting with the core of oneself, the visionary experience, and a rebirth. In the 21st century this classical initiation path is now conjoined with Jungian psychology which has become a new container into which the ancient wisdom and myths has been poured.

##### Gnosticism and Jungian Psychology
* [S2 Ep 15. Gnosticism & Jungian Psychology](https://alanmulhern.podbean.com/e/2-ep-15-gnosticism-jungian-psychology/) - [video](https://youtu.be/Eo2ajvvHOTE)
  > Jung knew he had to drop anchor into the historical depths since he felt instinctively that what he was discovering was not just another interesting theory but something that was archetypal in dimension and therefore could be found in many cultures throughout history and even lay in the historical unconscious despite repression and persecution - just as gnosticism lay buried in the sands beneath the Christian surface. This gives Jungian psychology an extraordinary depth, an anchor deep in the world psyche.

##### Gnostics and the Call
* [S2 Ep 16. The Gnostics and the Call](https://alanmulhern.podbean.com/e/s2-ep-16-the-gnostics-and-the-call/) - [video](https://youtu.be/Ml8Aba1OcVY)
  > “The symbol of the call is so fundamental to Eastern Gnosticism that it may be termed the religion of the call, sometimes termed the call from without which may illumine the mortal house” “I sent a call out into the world … He called with heavenly voice into the turmoil of the worlds”. (Hans Jonas)This “call” is the most powerful connection of Gnosticsm to Jungian psychology since the Self is also the source of the call - from the soul to the contemporary ego.

##### Fall of Sophia
* [S2 Ep 17. The Fall of Sophia and the Division of the Dark and the Light](https://alanmulhern.podbean.com/e/s2-ep12-the-fall-of-sophia-and-the-division-of-the-dark-and-the-light/) - [video](https://youtu.be/tW3jgGf8WuM)
  > “The struggle between Light and Darkness concentrates upon mankind, who becomes the main prize and battlefield of the two contending parties, in whom both sides have almost all their stakes: Light that of its own restoration, Darkness that of its very survival. This is the metaphysical centre of the Manichaean religion, and it enhances the deeds and destiny of the individual  to an absolute importance in the history of total existence”. Hans Jonas: The Gnostic Religion.

##### Gnosticism for our Time
* [S2 Ep 18.  Gnosticism for our time?](https://alanmulhern.podbean.com/e/gnosticism-for-our-time/) - [video](https://youtu.be/oNav4kiJsPc)
  > Gnostic themes of entrapment, profound alienation, the struggle for gnosis (knowledge, illumination), the battle between the light and the dark, the forces of evil and good, the illusions of the world, the devilish devices to distract one from truth are all archetypal themes that reoccur throughout history and across all cultures. Gnosticism, despite some of its excesses, speaks profound truth in wonderful poetry which once heard is unforgettable. They speak of the soul like no others:“In that world of darkness I dwelt thousands of myriads of years, and nobody knew of me that I was there. . . . Year upon year and generation upon generation I was there, and they did not know about me that I dwelt in their world.”In the modern era Gnostic motifs have surfaced in films such as Superman and the Matrix. This episode is the last in the Gnostic series. It finishes with a contemporary question: is there a gnostic dimension to the transgender question?


### Bethoven 3rd Symphony

* [S2 Ep 19. Beethoven 3rd symphony. Part 1. Battle and triumph.](https://alanmulhern.podbean.com/e/s2-ep-19-beethoven-3rd-symphony-part-1-battle-and-triumph/) - [video](https://youtu.be/hxjIUDUvMtg)
  > The opening movement of Beethoven’s 3rd is the first stage of the hero’s journey – struggle and victory. The hero myth has many variations in its death and resurrection themes. Mythologically it involves the bringing of new consciousness to humanity. It therefore represents in Beethoven’s day the revolts, insurrections and revolutions that were emerging in Europe and its colonies. However, Beethoven is tapping into an archetypal structure and therefore this music is as relevant now as it was then. For we too, in the contemporary age, live in a civilization in crisis and transition where radical change needs to be fought for.Individuals also seek their own renewal which is partially an independent activity, something, in the inner world – it’s not exactly a programme but an archetype, a template of immense possibility deep in the psyche. At its core the 3rd symphony demonstrates this within a death and rebirth archetype that takes music into the Romantic period and places the individual’s relation to the sublime or transcendent at the centre of consciousness.This is the first of three podcasts on Beethoven’s 3rd symphony.
